<?php
/*
Plugin Name: Sewn In Subscription Manager
Plugin URI: https://bitbucket.org/jupitercow/sewn-in-subscription-manager
Description: Unsubscribe from emails. This really just creates an interface for unsubscribing, it doesn't actually do anything more than set usermeta data that can be used by other functions.
Version: 0.1
Author: Jake Snyder
Author URI: http://Jupitercow.com/
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

------------------------------------------------------------------------
Copyright 2014 Jupitercow, Inc.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

if (! class_exists('sewn_subscription_manager') ) :

add_action( 'plugins_loaded', array('sewn_subscription_manager', 'plugins_loaded') );

add_action( 'init', array('sewn_subscription_manager', 'init') );

class sewn_subscription_manager
{
	/**
	 * Class prefix
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $prefix = __CLASS__;

	/**
	 * Current version of plugin
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $version = '0.1';

	/**
	 * Settings
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $settings = array();

	/**
	 * Holds the error/update action messages
	 *
	 * @since 	0.1
	 * @var 	string
	 */
	public static $messages;

	/**
	 * Initialize the Class
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function init()
	{
		add_filter( self::$prefix . '/settings/get_path', array(__CLASS__, 'helpers_get_path'), 1 );
		add_filter( self::$prefix . '/settings/get_dir',  array(__CLASS__, 'helpers_get_dir'), 1 );

		// Set up settings
		self::$settings = array(
			'path'     => apply_filters( self::$prefix . '/settings/get_path', __FILE__ ),
			'dir'      => apply_filters( self::$prefix . '/settings/get_dir',  __FILE__ ),
			'pages'    => array(
				'subscription' => array(
					'page_name'    => 'subscription',
					'page_title'   => 'Update Subscription',
					'page_content' => ''
				)
			),
			'types' => array(
				'announcements' => __("Announcements", self::$prefix),
				'promotions'    => __("Promotions", self::$prefix),
			),
		);

		self::$messages = array(
			'subscribe' => array(
				'key'     => 'action',
				'value'   => 'subscribe',
				'message' => __("Successfully subscribed", self::$prefix),
				'args'    => 'fade=true&page=' . self::$settings['pages']['subscription']['page_name']
			),
			'unsubscribe' => array(
				'key'     => 'action',
				'value'   => 'unsubscribe',
				'message' => __("Successfully unsubscribed", self::$prefix),
				'args'    => 'fade=true&page=' . self::$settings['pages']['subscription']['page_name']
			),
			'subscription_update' => array(
				'key'     => 'action',
				'value'   => 'subscription_update',
				'message' => __("Subscription successfully updated", self::$prefix),
				'args'    => 'fade=true&page=' . self::$settings['pages']['subscription']['page_name']
			)
		);
		self::$messages = apply_filters( self::$prefix . '/add_messages', self::$messages );

		// Add custom page content
		add_filter( 'the_content',                array(__CLASS__, 'the_content') );

		// Add a fake post for profile and register pages if they don't already exist
		add_filter( 'the_posts',                  array(__CLASS__, 'add_post') );
		add_filter( 'template_include',           array(__CLASS__, 'template_include'), 99 );

		// Process form and redirect as needed
		add_action( 'template_redirect',          array(__CLASS__, 'template_redirect') );

		// Add the subscription update action
		add_filter( self::$prefix . '/url/update_subscription',  array(__CLASS__, 'update_subscription_url') );
		add_filter( self::$prefix . '/link/update_subscription', array(__CLASS__, 'update_subscription_link') );

		// Add a dropdown to profile
		add_action( 'show_user_profile',          array(__CLASS__, 'user_profile') );
		add_action( 'edit_user_profile',          array(__CLASS__, 'user_profile') );

		// Process the profile dropdown
		add_action( 'personal_options_update',    array(__CLASS__, 'save_user_profile') );
		add_action( 'edit_user_profile_update',   array(__CLASS__, 'save_user_profile') );

		// Add a basic title/content interface for default use
		self::register_field_groups();
	}

	/**
	 * Creates an anchor link for updating email subscriptions
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 *
	 * @param	array	$args: an array holding the options
	 *			string	+ action: Action to be taken, either subscribe or unsubscribe
	 *			int		+ user_id: The ID of the user the link is for
	 *			string	+ type: Type of action to take, right now this doesn't matter
	 * @return	void
	 */
	public static function update_subscription_link( $args='' )
	{
		$defaults = array(
			'text'    => false,
			'title'   => false,
			'action'  => false,
			'class'   => 'button',
			'user_id' => false
		);
		$args = wp_parse_args( $args, $defaults );

		if (! $args['action'] )
		{
			$user = self::get_user($args['user_id']);
			$status = get_user_meta($user->ID, self::$prefix . '_status', true);
			$args['action'] = ( 'unsubscribed' == $status ) ? 'subscribe' : 'unsubscribe';
		}

		extract( $args, EXTR_SKIP );

		if (! $text )  $text  = ( 'subscribe' == $action ) ? apply_filters(self::$prefix . '/text/subscribe', "Subscribe") : apply_filters(self::$prefix . '/text/unsubscribe', "Unsubscribe");
		if (! $title ) $title = ( 'subscribe' == $action ) ? apply_filters(self::$prefix . '/title/subscribe', "Subscribe to Emails") : apply_filters(self::$prefix . '/title/unsubscribe', "Unsubscribe from Emails");
		$class .= " subscription_update subscription_" . $action;

		$url = apply_filters( self::$prefix . '/url/update_subscription', $args );
		echo '<a class="' . $class . '" href="' . $url . '" title="' . $title . '">' . $text . '</a>';
	}

	/**
	 * Creates a url for updating email subscriptions
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 *
	 * @param	array	$args: an array holding the options
	 *			string	+ action: Action to be taken, either subscribe or unsubscribe
	 *			int		+ user_id: The ID of the user the link is for
	 *			string	+ type: Type of action to take, right now this doesn't matter
	 * @return	void
	 */
	public static function update_subscription_url( $args='' )
	{
		$defaults = array(
			'action'  => 'unsubscribe',
			'user_id' => false,
			'type'    => 'all'
		);
		$args = wp_parse_args( $args, $defaults );
		extract( $args, EXTR_SKIP );

		$user = self::get_user($user_id);
		$hash = self::get_hash($user);
		return add_query_arg( array( 'eid' => $hash, 'action' => urlencode($action), 'type' => urlencode($type), 'uid' => $user->ID ), home_url('/'. self::$settings['pages']['subscription']['page_name'] .'/') );
	}

	/**
	 * Creates a hash for links to securely allow users access
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function get_hash( $user=false )
	{
		$user = self::get_user($user);
		if (! $user ) return false;

		$hash = hash('sha256', site_url() . $user->ID . $user->user_login . $user->data->user_pass);
		return $hash;
	}

	/**
	 * Test hash against user
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function test_hash( $submitted_hash, $user_id )
	{
		$user = get_user_by( 'id', $user_id );
		if (! $user ) return false;

		$hash = hash('sha256', site_url() . $user->ID . $user->user_login . $user->data->user_pass);
		if ( $hash == $submitted_hash ) return true;

		return false;
	}

	/**
	 * Get the user
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function get_user( $user_id=false )
	{
		$user = false;
		if ( $user_id && is_numeric($user_id) )
		{
			$user = get_user_by( 'id', $user_id );
		}
		elseif ( is_object($user_id) )
		{
			$user = $user_id;
		}
		elseif (! empty($_REQUEST['eid']) && ! empty($_REQUEST['uid']) && self::test_hash($_REQUEST['eid'], $_REQUEST['uid']) )
		{
			$user = get_user_by( 'id', $_REQUEST['uid'] );
		}
		else
		{
			$user = wp_get_current_user();
		}

		return $user;
	}

	/**
	 * On plugins_loaded test if we can use frontend_notifications
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function plugins_loaded()
	{
		// Have the login plugin use frontend notifictions plugin
		if ( apply_filters( self::$prefix . '/use_frontend_notifications', true ) )
		{
			if ( class_exists('frontend_notifications') )
			{
				#add_filter( 'frontend_notifications/queries', array(__CLASS__, 'add_notification_messages') );
			}
			else
			{
				add_filter( self::$prefix . '/use_frontend_notifications', '__return_false' );
			}
		}
	}

	/**
	 * Add this plugin's notification messages to the frontend_notifications plugin.
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	array $queries The modified queries for the frontend_notification plugin
	 */
	public static function add_notification_messages( $queries )
	{
		$queries = array_merge($queries, self::$messages);
		return $queries;
	}

	/**
	 * See if not register post exists and add it dynamically if not
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	object $posts Modified $posts with the new register post
	 */
	public static function add_post( $posts )
	{
		global $wp, $wp_query;

		// Check if the requested page matches our target, and no posts have been retrieved
		if (! $posts && array_key_exists(strtolower($wp->request), self::$settings['pages']) )
		{
			// Add the fake post
			$posts   = array();
			$posts[] = self::create_post( strtolower($wp->request) );

			$wp_query->is_page     = true;
			$wp_query->is_singular = true;
			$wp_query->is_home     = false;
			$wp_query->is_archive  = false;
			$wp_query->is_category = false;
			//Longer permalink structures may not match the fake post slug and cause a 404 error so we catch the error here
			unset($wp_query->query["error"]);
			$wp_query->query_vars["error"]="";
			$wp_query->is_404=false;
		}
		return $posts;
	}

	/**
	 * Create a dynamic post on-the-fly for the register page.
	 *
	 * source: http://scott.sherrillmix.com/blog/blogger/creating-a-better-fake-post-with-a-wordpress-plugin/
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	object $post Dynamically created post
	 */
	public static function create_post( $type )
	{
		// Create a fake post.
		$post = new stdClass();
		$post->ID                    = -1;
		$post->post_author           = 1;
		$post->post_date             = current_time('mysql');
		$post->post_date_gmt         = current_time('mysql', 1);
		$post->post_content          = self::$settings['pages'][$type]['page_content'];
		$post->post_title            = self::$settings['pages'][$type]['page_title'];
		$post->post_excerpt          = '';
		$post->post_status           = 'publish';
		$post->comment_status        = 'closed';
		$post->ping_status           = 'closed';
		$post->post_password         = '';
		$post->post_name             = self::$settings['pages'][$type]['page_name'];
		$post->to_ping               = '';
		$post->pinged                = '';
		$post->post_modified         = current_time('mysql');
		$post->post_modified_gmt     = current_time('mysql', 1);
		$post->post_content_filtered = '';
		$post->post_parent           = 0;
		$post->guid                  = home_url('/' . self::$settings['pages'][$type]['page_name'] . '/');
		$post->menu_order            = 0;
		$post->post_type             = 'page';
		$post->post_mime_type        = '';
		$post->comment_count         = 0;
		$post->filter                = 'raw';
		return $post;   
	}

	/**
	 * Make sure that the fake page uses the page templates and not single
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @param	string $template Template file path
	 * @return	array $template
	 */
	public static function template_include( $template )
	{
		global $wp, $wp_query;

		if ( is_page() && array_key_exists(strtolower($wp->request), self::$settings['pages']) )
		{
			acf_form_head();
			$new_template = locate_template( array( 'page-' . $wp->request . '.php', 'page.php' ) );
			if ( $new_template )
			{
				return $new_template;
			}
		}
		return $template;
	}

	/**
	 * Adds a support for unsubscribe links
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function template_redirect()
	{
		if ( is_page(self::$settings['pages']['subscription']['page_name']) )
		{
			if (! is_user_logged_in() && (empty($_GET['eid']) || empty($_GET['uid']) || ! self::test_hash($_GET['eid'], $_GET['uid'])) )
			{
				wp_redirect( home_url('/') );
				die;
			}

			if (! empty($_GET['action']) )
			{
				if ( 'unsubscribe' == $_GET['action'] )
				{
					update_user_meta( esc_sql($_GET['uid']), self::$prefix . '_status', 'unsubscribed' );
					do_action( 'frontend_notifications/add', "Successfully unsubscribed", '' );
				}
				elseif ( 'subscribe' == $_GET['action'] )
				{
					update_user_meta( esc_sql($_GET['uid']), self::$prefix . '_status', 'subscribed' );
					do_action( 'frontend_notifications/add', "Successfully subscribed", '' );
				}
			}
		}
	}

	/**
	 * Adds custom content
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	string $content The post content for login page with the login form a
	 */
	public static function the_content( $content )
	{
		if ( is_page(self::$settings['pages']['subscription']['page_name']) && is_main_query() && in_the_loop() )
		{
			$messages = $footer = '';
			$args = false;
			if (! empty($_GET['action']) )
			{
				$action = $_GET['action'];
				if (! apply_filters( self::$prefix . '/use_frontend_notifications', true ) && ! empty(self::$messages[$action]['message']) )
					$messages = '<p class="' . self::$prefix . '_messages alert alert-success">' . self::$messages[$action]['message'] . '</p>';
			}

			$field_groups = apply_filters( self::$prefix . '/field_groups', array('acf_customize_subscripition') );
			$user = self::get_user(); ?>
			<form role="form" id="<?php echo self::$prefix; ?>_form" method="post">
				<?php 
				// ACF Form
				acf_form( array(
					'post_id'      => 'user_'. $user->ID,
					'form'         => false,
					'field_groups' => $field_groups,
					'return'       => add_query_arg( 'action', 'subscription_update', get_permalink() ),
				) ); ?>
				<button type="submit" class="btn btn-default">Update</button>
			</form>
			<?php
		}

		return $content;
	}

	/**
	 * Adds a dropdown to the admin profile to subscribe/unsubscribe user
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 * /
	public static function user_profile( $user )
	{
		$key    = 'status';
		$status = get_user_meta( $user->ID, self::$prefix . '_' . $key, true );
		$types  = apply_filters( self::$prefix . '/types', self::$settings['types'] ); ?>
		<table class="form-table">
			<tr id="<?php echo self::$prefix; ?>-<?php echo $key; ?>" class="form-field field field_type-checkbox">
				<th valign="top" scope="row"><label for="<?php echo self::$prefix; ?>[<?php echo $key; ?>]"><?php _e("Unsubscribe from lists", self::$prefix); ?></label></th>
				<td>
					<ul class="acf-checkbox-list checkbox vertical list-group">
					<?php foreach ( $types as $type_key => $type_title ) : ?>
						<li class="list-group-item"><label><input id="<?php echo $key; ?>-<?php echo esc_attr($type_key); ?>" type="checkbox" class="checkbox" name="<?php echo self::$prefix; ?>[<?php echo $key; ?>][]" value="<?php echo esc_attr($type_key); ?>"<?php if ( is_array($status) && in_array($type_key, $status) ) echo ' checked="checked"'; ?>><?php echo esc_html($type_title); ?></label></li>
					<?php endforeach; ?>
					</ul>
					<span class="description"><?php _e("Check the boxes next to lists you want to unsubscribe from.", self::$prefix); ?></span>
				</td>
			</tr>
		</table>
		<?php
	}

	/**
	 * Process the profile dropdown
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function save_user_profile( $user_id )
	{
		if (! current_user_can('edit_user', $user_id) ) return false;

		$key = 'status';
		if ( isset($_POST[self::$prefix][$key]) )
		{
			update_user_meta( $user_id, self::$prefix . '_' . $key, $_POST[self::$prefix][$key] );
		}
		else
		{
			delete_user_meta( $user_id, self::$prefix . '_' . $key );
		}
	}

	/**
	 * Get the plugin path
	 *
	 * Calculates the path (works for plugin / theme folders). These functions are from Elliot Condon's ACF plugin.
	 *
	 * @since	0.1
	 * @return	void
	 */
	public static function helpers_get_path( $file )
	{
	   return trailingslashit( dirname($file) );
	}

	/**
	 * Get the plugin directory
	 *
	 * Calculates the directory (works for plugin / theme folders). These functions are from Elliot Condon's ACF plugin.
	 *
	 * @since	0.1
	 * @return	void
	 */
	public static function helpers_get_dir( $file )
	{
        $dir = trailingslashit( dirname($file) );
        $count = 0;

        // sanitize for Win32 installs
        $dir = str_replace('\\' ,'/', $dir);

        // if file is in plugins folder
        $wp_plugin_dir = str_replace('\\' ,'/', WP_PLUGIN_DIR); 
        $dir = str_replace($wp_plugin_dir, plugins_url(), $dir, $count);

        if ( $count < 1 )
        {
	       // if file is in wp-content folder
	       $wp_content_dir = str_replace('\\' ,'/', WP_CONTENT_DIR); 
	       $dir = str_replace($wp_content_dir, content_url(), $dir, $count);
        }

        if ( $count < 1 )
        {
	       // if file is in ??? folder
	       $wp_dir = str_replace('\\' ,'/', ABSPATH); 
	       $dir = str_replace($wp_dir, site_url('/'), $dir);
        }

        return $dir;
    }

	/**
	 * Add a basic interface for adding to front end forms, so we don't have to create them in the admin
	 *
	 * @author  Jake Snyder
	 * @since	0.1
	 * @return	void
	 */
	public static function register_field_groups()
	{
		if ( function_exists("register_field_group") )
		{
			$args = array(
				'id' => 'acf_customize_subscripition',
				'title' => apply_filters( 'acf/' . self::$prefix . '/group/title', 'Update Subscriptions' ),
				'fields' => array (),
				'location' => array (
					array (
						array (
							'param' => 'ef_user',
							'operator' => '==',
							'value' => 'all',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => -10,
			);

			$args['fields'][] = array(
				'key' => 'field_534e753f410f0',
				'label' => apply_filters( 'acf/' . self::$prefix . '/subscription/title', 'Update Subscriptions' ),
				'name' => self::$prefix . '/subscriptions',
				'type' => apply_filters( 'acf/' . self::$prefix . '/subscription/type', 'checkbox' ),
				'instructions' => apply_filters( 'acf/' . self::$prefix . '/subscription/instructions', 'Check the boxes next to lists you want to unsubscribe from.' ),
				'choices' => apply_filters( self::$prefix . '/types', self::$settings['types'] ),
				'default_value' => '',
				'layout' => 'vertical',
			);

			register_field_group( $args );
		}
	}
}

endif;